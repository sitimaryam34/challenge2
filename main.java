import com.sun.org.apache.xml.internal.security.utils.HelperNodeList;

import java.util.Scanner;
import java.io.File; //untuk inport file class
import java.io.IOException; //untuk heandle error
import java.util.List; //untuk membuat list
import java.util.ArrayList; //untuk membuat list array
public class main {
    public static void main(String[] args) {
       try {
            File myObj = new File("C:\\Temp\\direktori");
            if (myObj.createNewFile()) {
                System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori berikut : ");
                System.out.println("" + myObj.getName());
            } else {
                System.out.println("File siap digunakan, dan berada di " + myObj.getName());
            }
        }
           catch(IOException printStackTrace){
                System.out.println("Maaf terjadi kesalahan");
           }
        Scanner input = new Scanner(System.in);
        int Menu, menu;
        float Rata, Me, Mo;
        while (true) {
            //tampilan menu
            System.out.println("---------------------------------------------------------");
            System.out.println("                Aplikasi Pengolah Nilai Siswa");
            System.out.println("---------------------------------------------------------");
            System.out.println("Menu");
            System.out.println("1. Generate txt untuk menampilkan modus");
            System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, median");
            System.out.println("3. Generate  kedua file");
            System.out.println("0. Exit");
            System.out.println("---------------------------------------------------------");
            System.out.print("Pilih Menu : ");
            Menu = input.nextInt();

            if (Menu == 1) {

                System.out.println("---------------------------------------------------------");
                System.out.println("                Aplikasi Pengolah Nilai Siswa");
                System.out.println("---------------------------------------------------------");
                System.out.println("Berikut hasil pengolahan nilai siswa: ");
                //buat array list untuk modus
                List<Integer> nilai = new ArrayList<>();
                nilai.add(5);
                nilai.add(6);
                nilai.add(7);
                nilai.add(8);
                nilai.add(9);
                nilai.add(10);
                List<Integer> frekuensi = new ArrayList<>();
                frekuensi.add(9);
                frekuensi.add(12);
                frekuensi.add(3);
                frekuensi.add(15);
                frekuensi.add(8);
                frekuensi.add(7);
                // mencetak array list untuk modus
                System.out.println("Nilai  |   Frekuensi");
                System.out.print(" ");System.out.print(nilai.get(0));System.out.print("     |   "); System.out.println(frekuensi.get(0));
                System.out.print(" ");System.out.print(nilai.get(1));System.out.print("     |   "); System.out.println(frekuensi.get(1));
                System.out.print(" ");System.out.print(nilai.get(2));System.out.print("     |   "); System.out.println(frekuensi.get(2));
                System.out.print(" ");System.out.print(nilai.get(3));System.out.print("     |   "); System.out.println(frekuensi.get(3));
                System.out.print(" ");System.out.print(nilai.get(4));System.out.print("     |   "); System.out.println(frekuensi.get(4));
                System.out.print(nilai.get(5));System.out.print("     |   "); System.out.println(frekuensi.get(5));

                System.out.println("");
                System.out.println("Menu");
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Exit");
                System.out.println("---------------------------------------------------------");
                System.out.print("Pilih Menu : ");
                menu = input.nextInt();
                     if (Menu == 1) {
                     }
                    else{
                        System.exit(Menu);
                    }
            }
            else if (Menu == 2) {
                System.out.println("---------------------------------------------------------");
                System.out.println("                Aplikasi Pengolah Nilai Siswa");
                System.out.println("---------------------------------------------------------");
                System.out.println("Berikut hasil pengolahan nilai:");
                System.out.println("Mean   : 7,40");
                System.out.println("Median : 8");
                System.out.println("");
                System.out.println("Menu");
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Exit");
                System.out.println("---------------------------------------------------------");
                System.out.print("Pilih Menu : ");
                if (Menu == 1) {
                } else {
                    System.exit(Menu);
                }
            }
            else if (Menu == 3) {
                System.out.println("---------------------------------------------------------");
                System.out.println("                Aplikasi Pengolah Nilai Siswa");
                System.out.println("---------------------------------------------------------");
                List<Integer> nilai = new ArrayList<>();
                nilai.add(5);
                nilai.add(6);
                nilai.add(7);
                nilai.add(8);
                nilai.add(9);
                nilai.add(10);
                List<Integer> frekuensi = new ArrayList<>();
                frekuensi.add(9);
                frekuensi.add(12);
                frekuensi.add(3);
                frekuensi.add(15);
                frekuensi.add(8);
                frekuensi.add(7);
                // mencetak array list untuk modus
                System.out.println("Nilai  |   Frekuensi");
                System.out.print(" ");System.out.print(nilai.get(0));System.out.print("     |     ");System.out.println(frekuensi.get(0));
                System.out.print(" ");System.out.print(nilai.get(1));System.out.print("     |   ");System.out.println(frekuensi.get(1));
                System.out.print(" ");System.out.print(nilai.get(2));System.out.print("     |     ");System.out.println(frekuensi.get(2));
                System.out.print(" ");System.out.print(nilai.get(3));System.out.print("     |   ");System.out.println(frekuensi.get(3));
                System.out.print(" ");System.out.print(nilai.get(4));System.out.print("     |     ");System.out.println(frekuensi.get(4));
                System.out.print(nilai.get(5));System.out.print("     |     ");System.out.println(frekuensi.get(5));
                System.out.println("");
                System.out.println("Berdasarkan data di atas diperoleh hasil sebaran data nilai sebagai berikut:");
                System.out.println("Mean   : 7,40");
                System.out.println("Median : 8");
                System.out.println("Modus  : 8");
                System.out.println("");
                System.out.println("Menu");
                System.out.println("1. Kembali ke menu utama");
                System.out.println("0. Exit");
                System.out.println("---------------------------------------------------------");
                System.out.print("Pilih Menu : ");
                menu = input.nextInt();
                if (Menu == 1) {
                } else {
                    System.exit(Menu);
                }
            }
            else if (Menu == 0) {
                System.exit(Menu);
            }
        }
    }
}

//disaat mentok inilah jalan ninjaku:(
